## Library for the set-based task-priority inverse kinematics algorithm

**Description**  
The library allows to perform the set-based task-priority inverse kinematics control implemented by the [Lai Robotics](http://webuser.unicas.it/lai/robotica/); 
for more details about the algorithm you can read 
the work [*Assistive robot operated via P300-based brain computer interface*](https://ieeexplore.ieee.org/abstract/document/7989714).


---

**Instructions for Ubuntu**  
The library is written in C++ and is contained in a ROS package; then from your terminal:  
- go to your `catkin_ws/src` directory and download the lib_multitask `git clone https://lairobotics@bitbucket.org/lairobotics/lib_multitask.git`  
- build the lib_multitask with the ROS command `catkin_make`


---
