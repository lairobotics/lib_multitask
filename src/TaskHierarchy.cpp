#include <iostream>
#include <eigen3/Eigen/Core>
#include "LLkinematics.h"
#include <TaskHierarchy.h>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>





TaskHierarchy::TaskHierarchy(){
}

TaskHierarchy::~TaskHierarchy(){

//cout << "Distruttore task hierarchy\n";

}

void TaskHierarchy::clear(){

/*

	Clear the task hierarchy
*/


	hierarchy.clear();
	

}

void TaskHierarchy::setHierarchy(vector<Task> h){

/*

	Set the task hierarchy
*/



		this->hierarchy = h;


}



void TaskHierarchy::print(){


/*

	Print the task hierarchy
*/

vector<Task>::iterator it;

for(it=hierarchy.begin();it!=hierarchy.end();it++){

	it->print();
	cout << "\n";

}


}

bool TaskHierarchy::findTask(Task t){
	
		int N = this->hierarchy.size();
		bool found = false;
		
		for (int i=0;i<N;i++){
	
	
			if(this->hierarchy[i].getNumber() == t.getNumber())
	
			found = true;
	
		}
	
	return found;
	
	}

vector<Task> TaskHierarchy::getHierarchy(){

/*
	Get the task hierarchy
*/


return hierarchy;

}

int TaskHierarchy::size(){

/*
	Get the size og the task hierarchy
*/


return hierarchy.size();

}


Task TaskHierarchy::operator[](int i){


return hierarchy[i];


}

VectorXd TaskHierarchy::getTaskError(int i, VectorXd q){

/*

	Get the error of a task in the hierarchy
	
	input:
		
		int i			dim: 1		type of the task
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd error	dim: mx1	task error
*/

int id=-1;

for(int j=0;j<hierarchy.size();j++){


if(hierarchy[j].getType()==i)

	id=j;

}

if(id!=-1)
	return hierarchy[id].Error(q);
else{

	cout << "\nError in getTaskError(): the requested task is not in the current hierarchy\n";
	exit(0);
}

}


VectorXd TaskHierarchy::computeNSBsolution(VectorXd q){

/*

	Compute the task hierarchy solution.
	
	input:
	
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::VectorXd dq	dim: nx1	desired joint velocities

*/



int joints = q.size();
VectorXd dq(joints);
dq.setZero();

int N = hierarchy.size();


dq = hierarchy[0].computeSolution(q);



int size_curr=0;

MatrixXd I(joints,joints), Null(joints,joints);
I.setIdentity();


for(int i=1;i<N;i++){


size_curr = 0;
int nu=0;
	for(int j=0;j<i;j++){

		size_curr+=hierarchy[j].getTaskDim();
	
		nu=j+1;				
	}


MatrixXd augmentedJ(size_curr, joints);

int count = 0;

for(int k=0;k<nu;k++){

augmentedJ.block(count,0, hierarchy[k].getTaskDim(),joints) = hierarchy[k].Jacobian(q);

count+=hierarchy[k].getTaskDim();
	

}

/*
MatrixXd W(joints,joints);
W.setIdentity();


double error_norm = 0;
double max_out = 10;
int flag_sigma = 0;
double d, l, sigma_min;
int algorithm = 2;
*/

FullPivLU<MatrixXd> lu(augmentedJ);

//cout << augmentedJ.rows() << "\n";

if(augmentedJ.rows()>=joints-1){
	
	return dq;
	
	}



//Null = I - (dls_pinv(augmentedJ,W,error_norm, max_out, flag_sigma) * augmentedJ);
Null = I - (mypinv(augmentedJ) * augmentedJ);

FullPivLU<MatrixXd> u(Null);


MatrixXd W(joints,joints);
W.setIdentity();

double error_norm = hierarchy[i].Error(q).norm(); 
double max_out = 0.2;
int flag_sigma = 0;

//dq = dq + Null  * hierarchy[i].computeSolution(q);

MatrixXd Jtilde(joints, joints);
Jtilde = hierarchy[i].Jacobian(q) * Null;
double lambda = 0.0001;
double threshold = 0.01;
//dq = dq + dls_pinv(Jtilde,W,error_norm,max_out,flag_sigma) * hierarchy[i].getK() * (hierarchy[i].Error(q) - hierarchy[i].Jacobian(q) * dq); 
dq = dq + eig_pinv(Jtilde, threshold, lambda) * hierarchy[i].getK() * (hierarchy[i].Error(q) - hierarchy[i].Jacobian(q) * dq);


}



return dq;

}

void TaskHierarchy::insert(Task t){

/*

	Insert a task in the hierarchy
*/

hierarchy.push_back(t);

}

VectorXd TaskHierarchy::getTaskValue(int i, VectorXd q){

/*

	Get the value of a task in the hierarchy.
	
	input:
	
		int i			dim: 1		task type
		Eigen::VectorXd q	dim: nx1	joint positions	
		
	output:
	
		Eigen::VectorXd value	dim: mx1	task value
*/


	int id=-1;

for(int j=0;j<hierarchy.size();j++){


if(hierarchy[j].getType()==i)

	id=j;

}

if(id!=-1)
	return hierarchy[id].taskValue(q);
else{

	cout << "\nError in getTaskValue(): the requested task is not in the current hierarchy\n";
	exit(0);
}
}

void TaskHierarchy::setTaskParameters(int i, vector<double> p){

/*
	Set the paramenters vector for a task in the hierarchy.
	
	input:
		int i 			dim: 1			task type
		vector<double>		dim: variable		parameters vector	

*/


int id=-1;

for(int j=0;j<hierarchy.size();j++){


if(hierarchy[j].getType()==i)

	id=j;

}

if(id!=-1)

	hierarchy[id].setParams(p);

else{

	cout << "\nError in setTaskParameters(): the requested task is not in the current hierarchy\n";
	exit(0);
}





}

