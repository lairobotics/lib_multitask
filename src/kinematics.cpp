#include "LLkinematics.h"



using namespace Eigen;
using namespace std;

int i=0, j=0, k=0, z=0;

double **readDH2(int joints){


/*
	Initialize the DH table for a Jaco2 (7 DoF) arm
	
	input:
	
		int joints 	dim: 1		number of DoFs
		
	output:
	
		double **DH	dim: nx4	DH table


*/


    double **DH = allocateArray2d(joints, 4);
/*
    DH[0][0] = 0.0;
    DH[1][0] = 0.0;
    DH[2][0] = 0.0;
    DH[3][0] = 0.0;
    DH[4][0] = 0.0;
    DH[5][0] = 0.0;
    DH[6][0] = 0.0;

    DH[0][1] = 90.0/180.0*PI;
    DH[1][1] = 90.0/180.0*PI;
    DH[2][1] = 90.0/180.0*PI;
    DH[3][1] = 90.0/180.0*PI;
    DH[4][1] = 60.0/180.0*PI;
    DH[5][1] = 60.0/180.0*PI;
    DH[6][1] = 180.0/180.0*PI;

    DH[0][2] =  0.2755;
    DH[1][2] = -0.0098;
    DH[2][2] = -0.410;
    DH[3][2] = -0.0098;
    DH[4][2] = -0.2502;
    DH[5][2] = -0.0858;
    DH[6][2] = -0.0459;
*/	
	
	/* POLSO SFERICO*/
	
    DH[0][0] = 0.0;
    DH[1][0] = 0.0;
    DH[2][0] = 0.0;
    DH[3][0] = 0.0;
    DH[4][0] = 0.0;
    DH[5][0] = 0.0;
    DH[6][0] = 0.0;

    DH[0][1] = 90.0/180.0*PI;
    DH[1][1] = 90.0/180.0*PI;
    DH[2][1] = 90.0/180.0*PI;
    DH[3][1] = 90.0/180.0*PI;
    DH[4][1] = 90.0/180.0*PI;
    DH[5][1] = 90.0/180.0*PI;
    DH[6][1] = 0.0/180.0*PI;

    DH[0][2] =  0.2755;
    DH[1][2] = 0.0;
    DH[2][2] = -0.410;
    DH[3][2] = -0.0098;
    DH[4][2] = -0.3111;
    DH[5][2] = 0.0;
    DH[6][2] = 0.2638+ 0.02;
	
    return DH;
}



double **readDH1(int joints){

/*
	Initialize the DH table for the DexROV arm
	
	input:
	
		int joints 	dim: 1		number of DoFs
		
	output:
	
		double **DH	dim: nx4	DH table


*/

    double **DH = allocateArray2d(joints, 4);


    DH[0][0] = 0.0;
    DH[1][0] = 0.431;
    DH[2][0] = 0.1695;
    DH[3][0] = 0.0;
    DH[4][0] = 0.0;
    DH[5][0] = 0.0;
    

    DH[0][1] = 90.0/180.0*PI;
    DH[1][1] = 180.0/180.0*PI;
    DH[2][1] = 90.0/180.0*PI;
    DH[3][1] = 90.0/180.0*PI;		
    DH[4][1] = -90.0/180.0*PI;
    DH[5][1] = 0.0/180.0*PI;
    

    DH[0][2] =  0.3065;
    DH[0][2] =  0.210+0.069;
    DH[1][2] = 0.0;
    DH[2][2] =  0.0;
    DH[3][2] =  0.437;
    DH[4][2] =  0.0;
    DH[5][2] =  0.4695;
    
    ///////////////////////////////////
    
    
    DH[0][0] = 0.0;
    DH[1][0] = 0.4631;
    DH[2][0] = 0.0;
    DH[3][0] = 0.0;
    DH[4][0] = 0.0;
    DH[5][0] = 0.0;
    

    DH[0][1] = 90.0/180.0*PI;
    DH[1][1] = 0.0/180.0*PI;
    DH[2][1] = -90.0/180.0*PI;
    DH[3][1] = 90.0/180.0*PI;		
    DH[4][1] = -90.0/180.0*PI;
    DH[5][1] = 0.0/180.0*PI;
    

    DH[0][2] =  0.3065;
   // DH[0][2] =  0.0;
    DH[1][2] =  0.0;
    DH[2][2] =  0.0;
    DH[3][2] =  0.437;
    DH[4][2] =  0.0;
    DH[5][2] =  0.1695;
    

	
    return DH;

}





double **readDH(int joints, char file_txt[]){

/*
	Initialize the DH table for a Jaco2 (7 DoF) arm from txt file
	
	input:
	
		int joints 	dim: 1		number of DoFs
		char file_txt[]	dim: 1		txt file with the DH parameters
		
	output:
	
		double **DH	dim: nx4	DH table


*/

    double **DH = allocateArray2d(joints, 4);
    FILE *fp;
    double a, alpha, d;




    fp=fopen(file_txt,"r");

    if(fp==NULL){
        cout<<"error: no file 'jaco"<<joints<<"-DH' found"<<endl;
        exit(1);
    }

    for(i=0; i<joints; i++){
        fscanf(fp,"%lf %lf %lf", &a, &alpha, &d);
        DH[i][0] = a;
        DH[i][1] = alpha/180.0*PI;
        DH[i][2] = d;

    }

    fclose(fp);
    return DH;
}

double **elaborateDH(int joints, double *q, double **DH){


/*
	Add the fourth column to a DH table
	
	input:
	
		int joints 	dim: 1		number of DoFs
		double *q	dim: 1		joint positions array
		double **DH	dim: nx4	DH table
		
	output:
	
		double **DH	dim: nx4	DH table


*/



    for(i=0; i<joints; i++){
        DH[i][3] = q[i];
    }

    return DH;
}




/*********   	Functions for the direct kinematics computation      *********************************/

double ***elaborateT(double **DH, int joints){

    double ***T;

    T = Homogeneous_dh(joints, DH);

    return T;
}

double ***Homogeneous_dh(int joints, double **DH){

    double ***Tapp=allocateArray3d(4,4, joints);

    double *a = new double[joints];
    double *alpha = new double[joints];
    double *d = new double[joints];
    double *theta = new double[joints];

    for(i=0; i<joints; i++){
        a[i] = DH[i][0];
        alpha[i] = DH[i][1];
        d[i] = DH[i][2];
        theta[i] = DH[i][3];
    }

    double ***R;

    R = Rot_dh(alpha, theta, joints);

    double ***p = allocateArray3d(3,1, joints);

    for(z=0; z<joints; z++){

        p[0][0][z] = a[z] * cos(theta[z]);
        p[1][0][z] = a[z] * sin(theta[z]);
        p[2][0][z] = d[z];

    }

    for(z=0; z<joints; z++){
        for(i=0; i<3; i++){
            for(j=0; j<3; j++){
                Tapp[i][j][z] = R[i][j][z];
            }
        }
    }

    for(z=0; z<joints; z++){
        for(i=0; i<3; i++){
            Tapp[i][3][z] = p[i][0][z];
        }
    }

    for(z=0; z<joints; z++){

        Tapp[3][0][z] = 0;
        Tapp[3][1][z] = 0;
        Tapp[3][2][z] = 0;
        Tapp[3][3][z] = 1;

    }

    delete[] a;
    delete[] alpha;
    delete[] d;
    delete[] theta;
    
    deallocateArray3d(3,3,R);
    deallocateArray3d(3,1,p);
    
    return Tapp;
}

double ***Rot_dh(double *alpha, double *theta, int joints){

    double ***R = allocateArray3d(3,3, joints);

    double *ct = new double[joints];
    double *st = new double[joints];
    double *ca = new double[joints];
    double *sa = new double[joints];

    for(i=0; i<joints; i++){
        ct[i] = cos(theta[i]);
        st[i] = sin(theta[i]);
        ca[i] = cos(alpha[i]);
        sa[i] = sin(alpha[i]);
    }

    for(z=0; z<joints; z++){
       R[0][0][z] = ct[z];
       R[0][1][z] = -st[z]*ca[z];
       R[0][2][z] = st[z]*sa[z];

       R[1][0][z] = st[z];
       R[1][1][z] = ct[z]*ca[z];
       R[1][2][z] = -ct[z]*sa[z];

       R[2][0][z] = 0;
       R[2][1][z] = sa[z];
       R[2][2][z] = ca[z];
    }

    delete[] ct;
    delete[] st;
    delete[] ca;
    delete[] sa;
    
    return R;
}

double ***elaborateT0(double ***T, int joints){

    double ***T0 = allocateArray3d(4,4, joints);

    for(i=0; i<4; i++){
        for(j=0; j<4; j++){
            T0[i][j][0] = T[i][j][0];
        }
    }

    for(z=1; z<joints; z++){
        for(i=0; i<4; i++){
            for(j=0; j<4; j++){
                T0[i][j][z] = 0;
                for(k=0; k<4; k++){
                    T0[i][j][z] += T0[i][k][z-1] * T[k][j][z];
                }
            }
        }
    }

    return T0;
}


/*******************************************************************************/



/****************** Auxiliary functions *****************************************/


void printArray2d(int row, int col, double **array2d){

    for(i=0; i<row; i++){
        for(j=0; j<col; j++){
            cout.precision(3);
            cout<< array2d[i][j] << "   ";
        }
        cout<<"\n\n";
    }
}

void printArray3d(int row, int col, int depth, double ***array3d){

    for(z=0; z<depth; z++){
        printf("[z] = [%d]\n", z);
        for(i=0; i<row; i++){
            for(j=0; j<col; j++){
                cout<< array3d[i][j][z] << "  ";
            }
            cout<<endl;
        }
        cout<<endl;
    }
}

void printArray2d_from_Array3d(int row, int col, int joint, double ***array3d){


        printf("[z] = [%d]\n", joint-1);
        for(i=0; i<row; i++){
            for(j=0; j<col; j++){
                cout << array3d[i][j][joint-1] << "  ";
            }
            cout<<endl;
        }
        cout<<endl;

}

double ***allocateArray3d(int row, int col, int depth){

    double ***array3d = new double**[row];

    for(i=0; i<row; i++){
        array3d[i] = new double*[col];
            for(j=0; j<col; j++){
                array3d[i][j] = new double[depth];
            }
    }
    return array3d;
}

double **allocateArray2d(int row, int col){

    double **array2d = new double*[row];

    for(i=0; i<row; i++){
        array2d[i] = new double[col];
    }

    return array2d;
}

void deallocateArray3d(int row, int col, double ***array3d){

    for(i=0; i<row; i++){
        for(j=0; j<col; j++){
            delete[] array3d[i][j];
        }
        delete[] array3d[i];
    }
    delete[] array3d;
}

void deallocateArray2d(int row, double **array2d){

    for(i=0; i<row; i++){
        delete[] array2d[i];
    }

    delete[] array2d;
}

int countRows(char file_txt[]){
    char ch;
    FILE *fp;
    int rows=0;

    fp = fopen(file_txt, "r");
    if(fp==NULL){
        cout<<"\nError: no file <jaco#-DH> found!!!\n"<<endl;
        exit(1);
    }

    ch = getc(fp);

    while(ch !=EOF){

        if(ch=='\n'){
            rows++;
        }
        ch = getc(fp);
    }
    fclose(fp);
    return rows;
}

int sign(double x){
    if(x>=0)
        return 1;
    else
        return -1;
}

/****************************************************************************************************/





VectorXd rot2quat(Matrix3d R){

/*
	Transformation from rotation matrix to quaternion
	
	input:
	
		Eigen::Matrix3d R		dim: 3x3	rotation matrix
		
	output:
	
		Eigen::VectorXd e1	dim: 4x1	quaternion


*/


    double *e = new double[4];

    int signum1 = sign(R(2,1) - R(1,2));
    int signum2 = sign(R(0,2) - R(2,0));
    int signum3 = sign(R(1,0) - R(0,1));

    double argument1 = R(0,0) - R(1,1) - R(2,2) + 1.0;
    double argument2 = -R(0,0) + R(1,1) - R(2,2) + 1.0;
    double argument3 = -R(0,0) - R(1,1) + R(2,2) + 1.0;

    if(argument1 < 0.0 && argument1 > -pow(10,-12)){
   //     cout<<"WARNING: argument1 in function 'rot2quat' is negative and near zero: -10^(-12) < argument1 < 0"<<endl;
        argument1 = 0.0;
    }
    else if(argument1 < -pow(10,-12)){
     //   cout<<"WARNING: argument1 in function 'rot2quat' is negative and NOT near zero: argument1 < -10^(-12)"<<endl;
        argument1 = 0.0;
    }
    if(argument2 < 0.0 && argument2 > -pow(10,-12)){
       // cout<<"WARNING: argument2 in function 'rot2quat' is negative and near zero: -10^(-12) < argument2 < 0"<<endl;
        argument2 = 0.0;
    }
    else if(argument2 < -pow(10,-12)){
        //cout<<"WARNING: argument2 in function 'rot2quat' is negative and NOT near zero: argument2 < -10^(-12)"<<endl;
        argument2 = 0.0;
    }
    if(argument3 < 0.0 && argument3 > -pow(10,-12)){
      //  cout<<"WARNING: argument3 in function 'rot2quat' is negative and near zero: -10^(-12) < argument3 < 0"<<endl;
        argument3 = 0.0;
    }
    else if(argument3 < -pow(10,-12)){
      //  cout<<"WARNING: argument3 in function 'rot2quat' is negative and NOT near zero: argument3 < -10^(-12)"<<endl;
        argument3 = 0.0;
    }


    e[3] = 0.5*sqrt(R(0,0) + R(1,1) + R(2,2) +1.0);

    if(isnan(e[3]))
   
   
    e[3] = 0.0;

    if(signum1 >= 0)
        e[0] = 0.5*sqrt(argument1);
    else
        e[0] = -0.5*sqrt(argument1);

    if(signum2 >= 0)
        e[1] = 0.5*sqrt(argument2);
    else
        e[1] = -0.5*sqrt(argument2);

    if (signum3 >= 0)
        e[2] = 0.5*sqrt(argument3);
    else
        e[2] = -0.5*sqrt(argument3);

//cout <<"\t" << R(0,0) << "\t" << R(1,1) << "\t" << R(2,2) << "\n\n";

    VectorXd e1 = array1dtoVectorXd(e, 4);

   
    return e1;

}

Matrix3d quat2rot(VectorXd e){



/*
	Transformation from quaternion to rotation matrix
	
	input:
	
		Eigen::VectorXd e		dim: 4x1	quaternion
		
	output:
	
		Eigen::Matrix3d R		dim: 3x3	rotation matrix


*/



    double t[10];


    Matrix3d R;
	
/*
    t[0] = pow(e[0],2);  //x^2
    t[1] = pow(e[1],2);	 //y^2
    t[2] = pow(e[2],2);  //z^2
    t[3] = e[0]*e[1];	 //x*y
    t[4] = e[0]*e[2];	 //x*z
    t[5] = e[1]*e[2];	 //y*z
    t[6] = e[3]*e[0];	 //eta*x
    t[7] = e[3]*e[1];    //eta*y
    t[8] = e[3]*e[2];	 //eta*z


    R(0,0) = 1 - 2 * (t[1] + t[2]);
    R(0,1) = 2 * (t[3] - t[8]);
    R(0,2) = 2 * (t[4] + t[7]);

    R(1,0) = 2 * (t[3] + t[8]);
    R(1,1) = 1 - 2 * (t[0] + t[2]);
    R(1,2) = 2 * (t[5] - t[6]);

    R(2,0) = 2 * (t[4] - t[7]);
    R(2,1) = 2 * (t[5] + t[6]);
    R(2,2) = 1 - 2 * (t[0] + t[1]);  */


    t[0] = pow(e[0],2);  //x^2
    t[1] = pow(e[1],2);	 //y^2
    t[2] = pow(e[2],2);  //z^2
    t[3] = pow(e[3],2);  //eta^2	
    t[4] = e[0]*e[1];	 //x*y
    t[5] = e[0]*e[2];	 //x*z
    t[6] = e[1]*e[2];	 //y*z
    t[7] = e[3]*e[0];	 //eta*x
    t[8] = e[3]*e[1];    //eta*y
    t[9] = e[3]*e[2];	 //eta*z


    R(0,0) =  2 * (t[3] + t[0]) - 1;
    R(0,1) = 2 * (t[4] - t[9]);
    R(0,2) = 2 * (t[5] + t[8]);


    R(1,0) = 2 * (t[4] + t[9]);
    R(1,1) = 2 * (t[3] + t[1]) -1;
    R(1,2) = 2 * (t[6] - t[7]);


    R(2,0) = 2 * (t[5] - t[8]);
    R(2,1) = 2 * (t[6] + t[7]);
    R(2,2) = 2 * (t[3] + t[2]) - 1;


    return R;
}

Matrix3d rpy2rot(Vector3d rpy){


/*
	Transformation from roll-pitch-yaw to rotation matrix
	
	input:
	
		Vector3d rpy		dim: 3x1	roll, pitch and yaw angles
		
	output:
	
		Matrix3d R		dim: 3x3	rotation matrix


*/


    double phi = rpy[0];
    double theta = rpy[1];
    double psi = rpy[2];

    double cp = cos(psi);
    double ct = cos(theta);
    double cf = cos(phi);

    double sp = sin(psi);
    double st = sin(theta);
    double sf = sin(phi);

    Matrix3d R;

    R(0,0) = cf * ct;
    R(0,1) = -sf * cp + cf * st * sp;
    R(0,2) = sf* sp + cf * st * cp;

    R(1,0) = sf * ct;
    R(1,1) = cf * cp + sf * st * sp;
    R(1,2) = -cf * sp + sf * st * cp;

    R(2,0) = -st;
    R(2,1) = ct * sp;
    R(2,2) = ct * cp;
    
    
    R(0,0) = cp * ct;
    R(0,1) = -sp * cf + cp * st * sf;
    R(0,2) = sp* sf + cp * st * cf;

    R(1,0) = sp * ct;
    R(1,1) = cp * cf + sp * st * sf;
    R(1,2) = -cp * sf + sp * st * cf;

    R(2,0) = -st;
    R(2,1) = ct * sf;
    R(2,2) = ct * cf;

    return R;

}

VectorXd rpy2quat(Vector3d rpy){

/*
	Transformation from roll-pitch-yaw to quaternion
	
	input:
	
		Vector3d rpy		dim: 3x1	roll, pitch and yaw angles
		
	output:
	
		Eigen::VectorXd e	dim: 4x1	quaternion


*/



    Matrix3d R = rpy2rot(rpy);

    VectorXd e = rot2quat(R);

    return e;

}

Vector3d quat2rpy(VectorXd quat){

/*
	Transformation from quaternion ro roll-pitch-yaw
	
	input:
	
		double *quat		dim: 4x1	quaternion
		
	output:
	
		double *rpy		dim: 3x1	roll, pitch and yaw angles


*/

  Matrix3d R = quat2rot(quat);
  Vector3d rpy = rot2rpy_1(R);

  return rpy;

}

Vector3d rot2rpy_1(Matrix3d R){  

/*
	Transformation from rotation matrix ro roll-pitch-yaw with theta in (-pi/2, pi/2)
	
	input:
	
		double **R		dim: 3x3	rotation matrix
		
	output:
	
		double *rpy		dim: 3x1	roll, pitch and yaw angles


*/

    Vector3d rpy;

    double app = sqrt(pow(R(2,1),2) + pow(R(2,2),2));

    rpy[0] = atan2(R(1,0), R(0,0)); //phi (around z)
    rpy[1] = atan2(-R(2,0), app);    //theta (around y)
    rpy[2] = atan2(R(2,1), R(2,2)); //psi (around x)

    return rpy;
}

double *rot2rpy_2(double **R){  

/*
	Transformation from rotation matrix ro roll-pitch-yaw with theta in (pi/2, 3pi/2)
	
	input:
	
		double **R		dim: 3x3	rotation matrix
		
	output:
	
		double *rpy		dim: 3x1	roll, pitch and yaw angles


*/

    double *rpy = new double[3];

    double app = sqrt(pow(R[2][1],2) + pow(R[2][2],2));

    rpy[0] = atan2(-R[1][0], -R[0][0]);  //phi (around z)
    rpy[1] = atan2(-R[2][0], -app);      //theta (around y)
    rpy[2] = atan2(-R[2][1], -R[2][2]);  //psi (around x)

    return rpy;
}


VectorXd R2axis(Matrix3d R){


VectorXd axis(4);

axis(0) = acos( (R(0,0) + R(1,1) + R(2,2) - 1) / 2 );
axis(1) = 1/(2*sin(axis(0))) * ( R(2,1) - R(1,2) );
axis(2) = 1/(2*sin(axis(0))) * ( R(0,2) - R(2,0) );
axis(3) = 1/(2*sin(axis(0))) * ( R(1,0) - R(0,1) );


return axis;

}

Matrix3d skew(VectorXd v){


Matrix3d skew;

skew << 0, -v(2), v(1),
	v(2), 0, -v(0),
	-v(1), v(0), 0;
	
return skew;

}


void trapezoidal(double qi, double qf, double tf, double t, double &q, double &dq, double &ddq){


/*
	Generates a 1-dimensional trapezoidal velocity profile trajectory
	
	input:
	
		double qi		dim: 1		initial value	
		double qf		dim: 1		final value
		double tf		dim: 1		final time of the trajectory
		double t		dim: 1		current time
		double &q		dim: 1		output target position
		double &dq		dim: 1		output target velocity
		double &ddq		dim: 1		output target acceleration
							


*/


    double dq_c = 2*(qf-qi)/(tf*1.3);

    double tc = (qi - qf + dq_c*tf)/dq_c;
    double ddq_c = pow(dq_c,2)/(qi - qf + dq_c*tf);

    if(t>=0 && t<=tc){

         q = qi + 0.5 * ddq_c * pow(t,2);
         dq = ddq_c * t;
         ddq = ddq_c;

    }

    else if (t>tc && t<=tf-tc){

        q = qi + ddq_c*tc*(t - 0.5*tc);
        dq = ddq_c*tc;
        ddq = 0;

    }

    else if( t>tf-tc && t<=tf){

        q = qf - 0.5*ddq_c*pow(tf-t, 2);
        dq = -ddq_c*t + ddq_c*tf;
        ddq = -ddq_c;

    }

    if(qi==qf){

        q=qi;
        dq=0;
        ddq=0;

    }
}



/************** Auxiliary functions **********************************/

MatrixXd array2dtoMatrixXd(double **M, int rows, int cols){

    int i,j;
    MatrixXd A(rows, cols);

    for(i=0; i<rows; i++){
        for(j=0; j<cols; j++){
            A(i,j) = M[i][j];
        }
    }

  
    deallocateArray2d(rows, M);

    return A;
}

VectorXd array1dtoVectorXd(double *m, int size){

    int i;
    VectorXd a(size);

    for(i=0; i<size; i++){
       a(i) = m[i];
    }

    delete[] m;

    return a;
}

double *VectorXdtoarray1d(VectorXd m){

    int i, size = m.size();
    double *a = new double[size];

    for(i=0; i<size; i++){
       a[i] = m(i);
    }

    return a;
}

double *mask_Vrep2DH_Jaco6dof(double *qVrep, int joints){

    double *qDH =  new double[joints];

    qDH[0] = -qVrep[0];
    qDH[1] = qVrep[1] - 90.0/180.0*PI;
    qDH[2] = qVrep[2] - 270.0/180.0*PI;
    qDH[3] = qVrep[3];
    qDH[4] = qVrep[4] + 180.0/180.0*PI;
    qDH[5] = qVrep[5];

    return qDH;
}

double *mask_DH2Vrep_Jaco6dof(double *qDH, int joints){

    double *qVrep =  new double[joints];

    qVrep[0] = -qDH[0];
    qVrep[1] = qDH[1] + 90.0/180.0*PI;
    qVrep[2] = qDH[2] + 270.0/180.0*PI;
    qVrep[3] = qDH[3];
    qVrep[4] = qDH[4] - 180.0/180.0*PI;
    qVrep[5] = qDH[5];

    return qVrep;
}

VectorXd mask_Vrep2DH_Jaco7dof(VectorXd qVrep, int joints){

    VectorXd qDH(joints);

    qDH(0) = -qVrep(0);
    qDH(1) = qVrep(1) - 180.0/180.0*PI;
    qDH(2) = qVrep(2);
    qDH(3) = qVrep(3);
    qDH(4) = qVrep(4);
    qDH(5) = qVrep(5) + 180.0/180.0*PI;
    qDH(6) = qVrep(6);

/* POLSO SFERICO */

    qDH(0) = -qVrep(0);
    qDH(1) = qVrep(1) - 180.0/180.0*PI;
    qDH(2) = qVrep(2);
    qDH(3) = qVrep(3);
    qDH(4) = qVrep(4);
    qDH(5) = qVrep(5) - 180.0/180.0*PI;
    qDH(6) = -qVrep(6) - 0.0/180.0*PI;


    return qDH;
}




VectorXd mask_DH2Vrep_Jaco7dof(VectorXd qDH, int joints){

    VectorXd qVrep(joints);

    qVrep(0) = -qDH(0);
    qVrep(1) = qDH(1) + 180.0/180.0*PI;
    qVrep(2) = qDH(2);
    qVrep(3) = qDH(3);
    qVrep(4) = qDH(4);
    qVrep(5) = qDH(5) - 180.0/180.0*PI;
    qVrep(6) = qDH(6);



	/*POLSO SFERICO */
	
    qVrep(0) = -qDH(0);
    qVrep(1) = qDH(1) + 180.0/180.0*PI;
    qVrep(2) = qDH(2);
    qVrep(3) = qDH(3);
    qVrep(4) = qDH(4);
    qVrep(5) = qDH(5) + 180.0/180.0*PI;
    qVrep(6) = qDH(6);


    return qVrep;
}



VectorXd mask_DH2Gazebo(VectorXd qDH, int joints){

    VectorXd qVrep(joints);

    qVrep(0) = qDH(0) - PI/2;
    qVrep(1) = -qDH(1) -PI/2;
    qVrep(2) = -qDH(2) - PI/2;
    qVrep(3) = -qDH(3);
    qVrep(4) = -qDH(4);
    qVrep(5) = qDH(5);
    

    return qVrep;
}

VectorXd mask_Gazebo2DH(VectorXd qVrep, int joints){


    VectorXd qDH(joints);

    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = -qVrep(1) + PI/2;
    qDH(2) = -qVrep(2) + PI/2;
    qDH(3) = -qVrep(3);
    qDH(4) = -qVrep(4);
    qDH(5) = qVrep(5);
  
  
    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = -qVrep(1) + 1.1961;
    qDH(2) = qVrep(2) -  1.1961;
    qDH(3) = -qVrep(3);
    qDH(4) = -qVrep(4);
    qDH(5) = qVrep(5);
    
    
    /* REAL ARM*/
   
    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = qVrep(1) + 1.1961;
    qDH(2) = -qVrep(2) -  1.1961-PI/2;
    qDH(3) = qVrep(3);
    qDH(4) = qVrep(4);
    qDH(5) = qVrep(5);
    
    /* REAL ARM BTS (LEFT) */
    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = qVrep(1) + 1.1961;
    qDH(2) = -qVrep(2) -  1.1961 - PI/2;
    qDH(3) = qVrep(3);
    qDH(4) = qVrep(4);
    qDH(5) = qVrep(5);
    
	
    return qDH;
}


VectorXd mask_DH_Gazebo_velocity(VectorXd qdot, int joints){

VectorXd qdot_new(joints);

qdot_new(0) = qdot(0);
qdot_new(1) = -qdot(1);
qdot_new(2) = -qdot(2);
qdot_new(3) = -qdot(3);
qdot_new(4) = -qdot(4);
qdot_new(5) = qdot(5);

////////////////////

qdot_new(0) = qdot(0);
qdot_new(1) = -qdot(1);
qdot_new(2) = qdot(2);
qdot_new(3) = -qdot(3);
qdot_new(4) = -qdot(4);
qdot_new(5) = qdot(5);

/* REAL ARM */

qdot_new(0) = qdot(0);
qdot_new(1) = qdot(1);
qdot_new(2) = -qdot(2);
qdot_new(3) = qdot(3);
qdot_new(4) = qdot(4);
qdot_new(5) = qdot(5);


/* REAL ARM BTS (LEFT)*/
qdot_new(0) = qdot(0);
qdot_new(1) = qdot(1);
qdot_new(2) = -qdot(2);
qdot_new(3) = qdot(3);
qdot_new(4) = qdot(4);
qdot_new(5) = qdot(5);


return qdot_new;


}


VectorXd mask_Gazebo2DH_sim(VectorXd qVrep, int joints){


    VectorXd qDH(joints);

    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = -qVrep(1) + PI/2;
    qDH(2) = -qVrep(2) + PI/2;
    qDH(3) = -qVrep(3);
    qDH(4) = -qVrep(4);
    qDH(5) = qVrep(5);
  
  
    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = -qVrep(1) + 1.1961;
    qDH(2) = qVrep(2) -  1.1961;
    qDH(3) = -qVrep(3);
    qDH(4) = -qVrep(4);
    qDH(5) = qVrep(5);
    
    
    /* REAL ARM*/
   /*
    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = qVrep(1) + 1.1961;
    qDH(2) = -qVrep(2) -  1.1961-PI/2;
    qDH(3) = qVrep(3);
    qDH(4) = qVrep(4);
    qDH(5) = qVrep(5);
    
	*/
    return qDH;
}


VectorXd mask_DH_Gazebo_velocity_sim(VectorXd qdot, int joints){

VectorXd qdot_new(joints);

qdot_new(0) = qdot(0);
qdot_new(1) = -qdot(1);
qdot_new(2) = -qdot(2);
qdot_new(3) = -qdot(3);
qdot_new(4) = -qdot(4);
qdot_new(5) = qdot(5);

////////////////////

qdot_new(0) = qdot(0);
qdot_new(1) = -qdot(1);
qdot_new(2) = qdot(2);
qdot_new(3) = -qdot(3);
qdot_new(4) = -qdot(4);
qdot_new(5) = qdot(5);

/* REAL ARM */
/*
qdot_new(0) = qdot(0);
qdot_new(1) = qdot(1);
qdot_new(2) = -qdot(2);
qdot_new(3) = qdot(3);
qdot_new(4) = qdot(4);
qdot_new(5) = qdot(5);
*/


return qdot_new;


}






VectorXd mask_Gazebo2DH_right(VectorXd qVrep, int joints){


    VectorXd qDH(joints);

    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = -qVrep(1) + PI/2;
    qDH(2) = -qVrep(2) + PI/2;
    qDH(3) = -qVrep(3);
    qDH(4) = -qVrep(4);
    qDH(5) = qVrep(5);
  
  
    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = -qVrep(1) + 1.1961;
    qDH(2) = qVrep(2) -  1.1961 -PI/2;
    qDH(3) = -qVrep(3);
    qDH(4) = -qVrep(4);
    qDH(5) = qVrep(5);
    
    
    /* REAL ARM*/
   
    qDH(0) = qVrep(0) + PI/2;
    qDH(1) = -qVrep(1) + 1.1961;
    qDH(2) = qVrep(2) -  1.1961 - PI/2;
    qDH(3) = -qVrep(3);
    qDH(4) = qVrep(4);
    qDH(5) = qVrep(5);
    
	
    return qDH;
}


VectorXd mask_DH_Gazebo_velocity_right(VectorXd qdot, int joints){

VectorXd qdot_new(joints);

qdot_new(0) = qdot(0);
qdot_new(1) = -qdot(1);
qdot_new(2) = -qdot(2);
qdot_new(3) = -qdot(3);
qdot_new(4) = -qdot(4);
qdot_new(5) = qdot(5);

////////////////////

qdot_new(0) = qdot(0);
qdot_new(1) = -qdot(1);
qdot_new(2) = qdot(2);
qdot_new(3) = -qdot(3);
qdot_new(4) = -qdot(4);
qdot_new(5) = qdot(5);

/* REAL ARM */

qdot_new(0) = qdot(0);
qdot_new(1) = -qdot(1);
qdot_new(2) = qdot(2);
qdot_new(3) = -qdot(3);
qdot_new(4) = qdot(4);
qdot_new(5) = qdot(5);



return qdot_new;


}

VectorXd odom2ik(VectorXd pos_in_odom){

Matrix3d Rtot, Rz, Ry, Rx;
Vector3d pos_in_ik;

/*
double alpha = PI/2;	// around z
double beta = -PI/2;	// around y
double gamma = 0.0;	// around x
*/

double alpha = -PI/2;	// around z
double beta = 0;	// around y
double gamma = -PI/2;	// around x


Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;
  

pos_in_ik = Rtot.inverse() * pos_in_odom;

return pos_in_ik;

}

VectorXd ik2odom(VectorXd pos_in_ik){

Matrix3d Rtot, Rz, Ry, Rx;
Vector3d pos_in_odom;

/*
double alpha = PI/2;	// around z
double beta = -PI/2;	// around y
double gamma = 0;	// around x
*/

double alpha = -PI/2;	// around z
double beta = 0;	// around y
double gamma = -PI/2;	// around x


Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;
  

pos_in_odom = Rtot * pos_in_ik;

return pos_in_odom;

}


VectorXd ik2odom_rotation(VectorXd quaternion_in_ik){

Matrix3d Rtot, Rz, Ry, Rx, Rtemp;
VectorXd quaternion_in_odom(4);

Matrix3d R_ik;

R_ik = quat2rot(quaternion_in_ik);

double alpha = PI/2;	// around z
double beta = -PI/2;	// around y
double gamma = 0;	// around x

Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;
  


Rtemp = Rtot.inverse() * R_ik;

quaternion_in_odom = rot2quat(Rtemp);

return quaternion_in_odom;

}

VectorXd odom2ik_rotation(VectorXd quaternion_in_odom){

Matrix3d Rtot, Rz, Ry, Rx, Rtemp;
VectorXd quaternion_in_ik(4);

Matrix3d R_odom;

R_odom = quat2rot(quaternion_in_odom);

double alpha = PI/2;	// around z
double beta = -PI/2;	// around y
double gamma = 0;	// around x

Rz << cos(alpha), -sin(alpha), 0,
      sin(alpha), cos(alpha), 0,
      0,0,1;
      
Ry << cos(beta), 0, sin(beta),
      0,1,0,
      -sin(beta),0,cos(beta);
      
Rx << 1, 0, 0,
      0, cos(gamma), -sin(gamma),
      0, sin(gamma), cos(gamma);
      
Rtot = Rz * Ry * Rx;
  


Rtemp = Rtot * R_odom;

quaternion_in_ik = rot2quat(Rtemp);

return quaternion_in_ik;


}



Vector3d GetEEPosition(VectorXd q)
{

Vector3d position;
int joints = q.size();

double ***T = jaco_directkinematics(q);


for(int i=0;i<3;i++)

	position(i) = T[i][3][joints-1];

return position;



}

VectorXd GetEEQuaternion(VectorXd q){

VectorXd quaternion(4);
Matrix3d R;
int joints = q.size();

double ***T = jaco_directkinematics(q);



for(int i=0;i<3;i++){

	for(int j=0;j<3;j++){
	
		R(i,j) = T[i][j][joints-1];
	}
}


quaternion = rot2quat(R);


return quaternion;

}

VectorXd VelocitySaturation(VectorXd dq){

int joints = dq.size();

VectorXd saturated(joints);
VectorXd max_velocity(joints);
double min_alfa=990.0, alfa;
double scale = 3.0;

max_velocity(0) = 0.2124*scale;	max_velocity(1) = 0.2273*scale;	max_velocity(2) = 0.2042*scale;
max_velocity(3) = 0.2151*scale;	max_velocity(4) = 0.2083*scale;	max_velocity(5) = 0.3138*scale;

bool saturate = false;

for(int i=0;i<joints;i++){

	if(abs(dq(i)) > max_velocity(i)){
	
		saturate = true;
		
	//	cout << "\nSaturating joint velocity";
	
		alfa = max_velocity(i) / abs(dq(i));
	
		if(alfa <= min_alfa){
		
		
			min_alfa = alfa;
		}
	
	}


}




if(saturate)
{

	for(int i=0;i<joints;i++){

		saturated(i) = min_alfa * dq(i);

	}

}

else

	saturated = dq;

return saturated;



}



MatrixXd armbase2ik(MatrixXd T_in){



Matrix3d R;
MatrixXd T(4,4), T_out(4,4);
		
R <<  -1, 0, 0,
       0, 0, -1,
       0, -1, 0;
	

Vector3d translation;
translation << 0,0,0;
	
T.block(0,0,3,3) = R;
T.block(0,3,3,1) = translation;
T.row(3) << 0,0,0,1;

T_out = T * T_in;

return T_out;


}


MatrixXd ik2armbase(MatrixXd T_in){


Matrix3d R;
MatrixXd T(4,4), T_out(4,4);
		
R <<  -1, 0, 0,
       0, 0, -1,
       0, -1, 0;
	

Vector3d translation;
translation << 0,0,0;
	
T.block(0,0,3,3) = R;
T.block(0,3,3,1) = translation;
T.row(3) << 0,0,0,1;

T_out = T.inverse() * T_in;

return T_out;


}


/******************************************************************/

VectorXd quatError(VectorXd ed, VectorXd e){


/*
	Quaternion error
	
	input:
	
		Eigen::VectorXd ed		dim: 4x1	desired quaternion
		Eigen::VectorXd e		dim: 4x1	current quaternion
		
	output:
	
		Eigen::Vector3d eo		dim: 3x1	quaternion error


*/

        int i=0;
        Vector3d ed_app;
        Vector3d e_app;
        Vector3d eo;
        

        for(i=0; i<3; i++){
            ed_app(i) = ed(i);
            e_app(i) = e(i);
        }
        
        
	double etatilde = e(3)*ed(3) +e_app.transpose() * ed_app;
        eo = e(3)*ed_app - ed(3)*e_app - ed_app.cross(e_app);
        double angle = atan2(eo.norm(), etatilde);
	
	if(angle > PI/2){
		
		eo = -eo;
		
	}

        return eo;
}
