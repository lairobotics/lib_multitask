#include <iostream>
#include <stdlib.h>
#include "LLkinematics.h"
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <math.h>

using namespace std;
using namespace Eigen;


MatrixXd mypinv(MatrixXd Jac){

    MatrixXd Jpinv;
   
    MatrixXd JacTran = Jac.transpose();
    MatrixXd Japp = Jac*JacTran;
    Jpinv = JacTran*Japp.inverse();

    return Jpinv;
}

MatrixXd mypinv3(MatrixXd Jac){

    MatrixXd Jpinv;
   
    MatrixXd JacTran = Jac.transpose();
    MatrixXd Japp = JacTran*Jac;
    Jpinv = Japp.inverse()*JacTran;

    return Jpinv;
}




MatrixXd mypinvScaled(MatrixXd Jac, MatrixXd W){

    MatrixXd Jpinv;

    MatrixXd Winv = W.inverse();

    MatrixXd JacTran = Jac.transpose();
    MatrixXd Japp = Jac*Winv*JacTran;

    Jpinv = Winv*JacTran*Japp.inverse();

    return Jpinv;
}

VectorXd bubble_sort(VectorXd v){


    for(int i=0; i<v.size()-1; i++){
        for(int j=0; j<v.size()-1; j++){
            if(v(j)> v(j+1)){
                double app = v(j+1);
                v(j+1) = v(j);
                v(j) = app;
            }
        }
    }

    return v;
}

bool any(MatrixXd J){
	int i,j;
	int rowJ = J.rows();
	int colJ = J.cols();

	for(i=0; i<rowJ; i++)
		for(j=0; j<colJ; j++)
			if(J(i,j)>0)
				return true;
		
	return false;
}

MatrixXd mypinv1(MatrixXd J, MatrixXd W, int flag_algorithm, double error_norm, double max_out, int flag_sigma, double &d, double &l, double &sigma_min){
	int i=0;
	//	double d,l, sigma_min;
	sigma_min=0;
	
	
	int rowJ = J.rows();
	int colJ = J.cols();
	MatrixXd pinvJ(colJ, rowJ);
	
	

  //Control if W is positive definite with eigValues ---------------------//
		EigenSolver<MatrixXd> es(W);	
		VectorXd eigValues_W(rowJ);
		complex<double> eigW;
		for(i=0; i<rowJ; i++){
			eigW = es.eigenvalues().col(0)[i];
			eigValues_W(i) = eigW.real();
		}
		
		

		for(i=0; i<eigValues_W.size(); i++){
			if(eigValues_W(i)<0){
				cout<<"Error: W must be positive definite"<<endl;
				return pinvJ = mypinv(J);
			}
		}	
		
		
  //----------------------------------------------------------------------//
	
  //Control if W is positive definite with SVD ---------------------------//	
		/*
		JacobiSVD<MatrixXd> svd(J, ComputeThinU | ComputeThinV);
		VectorXd Wsvd = svd.singularValues();

		for(i=0; i<Wsvd.size(); i++){
			if(Wsvd(i)<0){
				cout<<"Error: W must be positive definite"<<endl;
				return pinvJ = mypinv(J);
			}
		} */
  //----------------------------------------------------------------------//

  //Control if there are errors ------------------------------------------//		
		if(max_out <= 0){
			cout<<"\nError in mypinv1: max_out= "<<max_out<<endl;
			cout<<"check: max_out needs to be positive\n\n";
			exit(1);
		}

		if(flag_algorithm>1 && error_norm<0){
			cout<<"\nError in mypinv1: error_norm= "<<error_norm<<endl;
			cout<<"check: error_norm needs to be positive\n\n";
			exit(2);
		}
  //----------------------------------------------------------------------//	

  //Setting Damping Factor -----------------------------------------------//
		if(flag_algorithm==1)
			d = 1.0/max_out;
		else
			d = error_norm/max_out;
  //----------------------------------------------------------------------//

  //Main Condition IF-ELSE to verify if J is null ------------------------//
		if(any(J)){
		
		
			MatrixXd JWJ = J*W*J.transpose();
		
		//eigValues of JWJ Matrix +++++++++++++++++++++++++++++++++++++//	
			EigenSolver<MatrixXd> es(JWJ);
			VectorXd eigValues(rowJ);
			complex<double> app;
			for(i=0; i<rowJ; i++){
				app = es.eigenvalues().col(0)[i];
				eigValues(i) = app.real();
			}
			eigValues = bubble_sort(eigValues);
			
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
			
		//Initialization of sigma_min +++++++++++++++++++++++++++++++++//
			if(flag_sigma != 0){
			
				for(i=0; i<eigValues.size(); i++){		
					if(eigValues(i) != 0){
						sigma_min = sqrt(eigValues(i));
						break;
					}
				}
			}
			else{
			
				if(eigValues(0) <= pow(10,-15)){
					
					sigma_min = 0;
				//	cout << eigValues(0) << "\n\n\n";	
				}
				
				else{
					
					sigma_min = sqrt(eigValues(0));
				//	cout << "check2\n\n\n";
				}
			}
			
				
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//	

			if(eigValues(0)==0 && d==0){
				pinvJ = pinvJ.setZero();				
			}
			else{

			  //Setting factor "l" if flag_algorith is 2 (IF) +++++++++++++++++//
			  
					if(flag_algorithm==2){
					
						if(sigma_min>d)
							l = 0.0;
						else if(sigma_min <= d/2){
							l = 0.5*d;
					//		cout << "Damping! " <<rand() << "\n";
							}
						else{
							l = sqrt(sigma_min*(d-sigma_min));
					//		cout << "Damping! " << rand() << "\n";
						    }
						    
						    
					}
			  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

			  //Setting factor "l" if flag_algorith is 1 or 3 (ELSE) +++++++++++++++++//
					else{
						if(eigValues(0)==0 && sigma_min>d && flag_sigma!=0){
						//	cout<<"\n Warning in mypinv1: exception in algorithms 1 & 3";
							l = sqrt(sigma_min*(d-sigma_min));
						}
						else if(eigValues(0)==0)
							l = 1.0*pow(10.0,-7.0);
						else if(sigma_min>d)
							l = 0.0;
						else
							l = sqrt(sigma_min*(d-sigma_min));
					}
			  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
				
				MatrixXd I;
				I = I.setIdentity(rowJ,rowJ);
				MatrixXd M1 = W*J.transpose();
				MatrixXd M2 = (l*l*I + JWJ).inverse();
				
				pinvJ = M1*M2;
			//	pinvJ = W*J.transpose()/(l*l*I + JWJ);
			}
			//if(sigma_min<=d)
			//	cout<<"Warning in mypinv1: task singular (sigma_min: "<<sigma_min<<"), damped inverse used"<<endl;
		}
	//If the input matrix is null do nothing only transpose it ++++++++++++++++++//
		else{
			pinvJ = J.transpose();
		}
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//	
  //----------------------------------------------------------------------//
/*	cout<<"factor d = "<<d<<endl;
	cout<<"sigma min = "<<sigma_min<<endl;
	cout<<"lambda = "<<l<<endl;

*/	

	return pinvJ;

}


MatrixXd dls_pinv(MatrixXd J, MatrixXd W, double error_norm, double max_out, int flag_sigma){
	int i=0;
	double d,l, sigma_min;
	sigma_min=0;
	
	
	int rowJ = J.rows();
	int colJ = J.cols();
	MatrixXd pinvJ(colJ, rowJ);
	
	

  //Control if W is positive definite with eigValues ---------------------//
		EigenSolver<MatrixXd> es(W);	
		VectorXd eigValues_W(rowJ);
		complex<double> eigW;
		for(i=0; i<rowJ; i++){
			eigW = es.eigenvalues().col(0)[i];
			eigValues_W(i) = eigW.real();
		}
		
		

		for(i=0; i<eigValues_W.size(); i++){
			if(eigValues_W(i)<0){
				cout<<"Error: W must be positive definite"<<endl;
				return pinvJ = mypinv(J);
			}
		}	
		
	

  //Control if there are errors ------------------------------------------//		
		if(max_out <= 0){
			cout<<"\nError in mypinv1: max_out= "<<max_out<<endl;
			cout<<"check: max_out needs to be positive\n\n";
			exit(1);
		}

		if(error_norm<0){
			cout<<"\nError in mypinv1: error_norm= "<<error_norm<<endl;
			cout<<"check: error_norm needs to be positive\n\n";
			exit(2);
		}
  //----------------------------------------------------------------------//	

  //Setting Damping Factor -----------------------------------------------//
		
			d = error_norm/max_out;
		//	d = 1/max_out;
  //----------------------------------------------------------------------//

  //Main Condition IF-ELSE to verify if J is null ------------------------//
		if(any(J)){
		
	
			MatrixXd JWJ = J*W*J.transpose();
		
		//eigValues of JWJ Matrix +++++++++++++++++++++++++++++++++++++//	
			EigenSolver<MatrixXd> es(JWJ);
			VectorXd eigValues(rowJ);
			complex<double> app;
			for(i=0; i<rowJ; i++){
				app = es.eigenvalues().col(0)[i];
				eigValues(i) = app.real();
			}
			eigValues = bubble_sort(eigValues);
			
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
			
		//Initialization of sigma_min +++++++++++++++++++++++++++++++++//
			if(flag_sigma != 0){
			
				for(i=0; i<eigValues.size(); i++){		
					if(eigValues(i) != 0){
						sigma_min = sqrt(eigValues(i));
						break;
					}
				}
			}
			else{
			
				if(eigValues(0) <= pow(10,-15)){
					
					sigma_min = 0;
				
				}
				
				else{
					
					sigma_min = sqrt(eigValues(0));
				
				}
			}
			
			//	cout << "sigma min: " << sigma_min << "\n\n";
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//	

			if(eigValues(0)==0 && d==0){
				pinvJ = pinvJ.setZero();				
			}
			else{

			
			
						if(sigma_min>d)
							l = 0.0;
						else if(sigma_min < d/2){
						
							l = d/2;
			
							}
						else{

							l = sqrt(sigma_min*(d-sigma_min));
					
						    }
						    
					
		
				MatrixXd I;
				I = I.setIdentity(rowJ,rowJ);
				MatrixXd M1 = W*J.transpose();
				MatrixXd M2 = (l*l*I + JWJ).inverse();
				
				pinvJ = M1*M2;
				
				
			}
		
		}
	//If the input matrix is null do nothing only transpose it ++++++++++++++++++//
		else{
			pinvJ = J.transpose();
		}


	return pinvJ;

}
