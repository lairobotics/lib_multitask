#include "LLkinematics.h"

using namespace Eigen;
using namespace std;

const int J_row = 6;
const int p_row = 3;
const int p_col = 1;
const int z_row = 3;
const int z_col = 1;

MatrixXd J_oripos(VectorXd q){

/* 
	Jacobian for the end-effector configuration (position+orientation) task.
	
	input:
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		Eigen::MatrixXd J	dim: 6xn	jacobian matrix

*/


	int i,j,k;
	int joints = q.size();

	double **J = allocateArray2d(J_row, joints);
	double ***T0 = jaco_directkinematics(q);
	double **p = allocateArray2d(p_row, joints+1);  //position array
	double **z = allocateArray2d(z_row, joints+1);  //z array

	z[0][0] = 0;
	z[1][0] = 0;
	z[2][0] = 1;

	p[0][0] = 0;
	p[1][0] = 0;
	p[2][0] = 0;

	for(k=1; k<=joints; k++){
		for(i=0; i<p_row; i++){

				p[i][k] = T0[i][3][k-1];
				z[i][k] = T0[i][2][k-1];

		}
	}



	double **p_J = allocateArray2d(p_row, joints);  //position difference array

	//*************Processing position difference array*************
	for(j=0; j<joints; j++){
		for(i=0; i<p_row; i++){
			p_J[i][j] = p[i][joints] - p[i][j];
		//	cout << "ee:\n\n" << p[i][j] << "\n";
		}
	}


	double *Japp = new double[J_row];  //support variable Japp

	for(i=0; i<joints; i++){

		Japp = LinearAndAngular(p_J,z, i);
		for(j=0; j<J_row; j++){
			J[j][i] = Japp[j];
		}
	}

	for(i=0; i<J_row; i++){
		for(j=0; j<joints; j++){
			if((J[i][j]>=0 && J[i][j] < pow(10, -6)) || (J[i][j]<0 && J[i][j] > -pow(10, -6)))
				J[i][j]=0;
		}
	}
	
	MatrixXd J_out(6,joints);
	
	for(i=0;i<6;i++)
		for(j=0;j<joints;j++)
		
			J_out(i,j) = J[i][j];
			
	deallocateArray2d(6, J);
	deallocateArray2d(3, p);
	deallocateArray2d(3, z);
	deallocateArray2d(3, p_J);
	deallocateArray3d(4, 4, T0);
	delete[] Japp;

 	return J_out;
}


MatrixXd J_pos(VectorXd q){

/* Jacobian of the end-effector position task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 3xn		Jacobian matrix
	

*/

    int i=0, j=0;
    int joints = q.size();
    MatrixXd Jx(3, joints);

    MatrixXd J(6,joints);
    J = J_oripos(q);


    Jx = J.block(0,0,3,joints);


    return Jx;
}


MatrixXd J_ori(VectorXd q){

/* Jacobian of the end-effector orientation task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 3xn		Jacobian matrix
	

*/

     int i=0, j=0;
    int joints = q.size();
    MatrixXd Jx(3, joints);

    MatrixXd J(6,joints);
    J = J_oripos(q);

    Jx = J.block(3,0,3,joints);

    return Jx;
}




MatrixXd J_obst_wrist(Vector3d p_obst, VectorXd q){

/* Jacobian of the obstacle avoidance at wrist task.

 input: 
 
 	Eigen::Vector3d p_obst		dim: 3x1		x,y,z coordinates of the obstacle
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 1xn		Jacobian matrix
	

*/


    int joints = q.size();
    MatrixXd Jx(1, joints);
    int i=0, j=0;

    VectorXd qDH =q;

    
    MatrixXd T0(4,4);
    T0 = directkinematics(qDH);

    Vector3d eta_ee1;

    for(i=0; i<3; i++)
        eta_ee1(i) = T0(i,3);


    double dist = sqrt((p_obst - eta_ee1).transpose() * (p_obst - eta_ee1));

    MatrixXd J(3,joints);
    J = J_pos(qDH);

  
  
   if(dist < pow(10,-3))
        dist = pow(10,-3);

    Jx = - ((p_obst - eta_ee1).transpose()/dist)*J;

    return Jx;

}


MatrixXd J_obst_elbow(Vector3d p_obst, VectorXd q, int id){

/* Jacobian of the obstacle avoidance at elbow task.

 input: 
 
 	Eigen::Vector3d p_obst		dim: 3x1		x,y,z coordinates of the obstacle
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim 1			index of the elbow link
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 1xn		Jacobian matrix
	

*/


    int joints = q.size();
    MatrixXd Jx(1, joints);
    int i=0, j=0;

    VectorXd qDH =q;

    
    MatrixXd T0(4,4);
    T0 = directkinematics(qDH,id);
    Vector3d eta_ee1;

    for(i=0; i<3; i++)
        eta_ee1(i) = T0(i,3);


    double dist = sqrt((p_obst - eta_ee1).transpose() * (p_obst - eta_ee1));

    MatrixXd J(3,joints);
    J = J_pos(qDH);

  
  
   if(dist < pow(10,-3))
        dist = pow(10,-3);

    Jx = - ((p_obst - eta_ee1).transpose()/dist)*J;

    return Jx;

}


MatrixXd J_jointlimit(VectorXd q, int id){


/* Jacobian of the Joint limit  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim: 1			joint index
	
	
 output:
 
 	Eigen::MatrixXd	J		dim: 1xn		Jacobian matrix
	

*/


MatrixXd J(1,q.size());

if(id >= q.size()){

	cout << "\n[J_jointlimit function]: the system has " << q.size() << " joints, requested id = " << id+1 << endl; 
	exit(0);

}


for(int i=0;i<q.size();i++){

	if(i==id)
	
		J(0,i) = 1;
		
	else
	
		J(0,i) = 0;


}



return J;

}



    
    
MatrixXd J_manipulability(VectorXd q){

/* Jacobian of the arm manipulability  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	J		dim: 1xn		Jacobian matrix
	

*/





int i=0, j=0, joints = q.size();

MatrixXd J(1,joints);

       
        VectorXd qi(joints);
        double delta_q = 0.1;
        double w = 0, wi = 0;
        for(i=0; i< joints; i++){
            for(j=0; j<joints; j++){
                if(j==i)
                    qi(j) = q(j) + delta_q;
                else
                    qi(j) = q(j);
            }
            
            w = value_manipulability(q)(0);
            
            wi = value_manipulability(qi)(0);
            J(0,i) = (wi - w)/delta_q;
        }

return J;

}



MatrixXd J_wall(Vector3d p1, Vector3d p2, Vector3d p3, Vector3d p_d, VectorXd q){


/* Jacobian of the virtual plane task.

 input: 
 
 	Eigen::Vector3d p1		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p2		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p3		dim: 3x1		x,y,z coordinates of a point belonging to the plane
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::MatrixXd	Jx		dim: 1xn		Jacobian matrix
	

*/



int joints = q.size();
MatrixXd Jx(1, joints);
int i=0, j=0;

VectorXd qDH =q;


MatrixXd T(4, 4);
T = directkinematics(qDH);

Vector3d pos;

for(i=0; i<3; i++)
	pos(i) = T(i,3);


MatrixXd J(3,joints);
   
J = J_pos(qDH);
   
Vector3d plane_normal;


plane_normal << ((p2-p1).cross(p3-p1)) / ((p2-p1).cross(p3-p1)).norm();

double distance = plane_normal.transpose() * (pos - p1);


//Vector3d p_primo = abs(plane_normal.transpose()*(pos-p1))*plane_normal;




if( distance > 0){

Jx =  plane_normal.transpose() * J;
//Jx = ((pos-p_primo).transpose() / (pos-p_primo).norm()) * J;

}


else{

Jx = - plane_normal.transpose() * J;
//Jx = - ((pos-p_primo).transpose() / (pos-p_primo).norm()) * J;

}




return Jx;

}




MatrixXd J_fov(Vector3d p,  VectorXd q){
/*
int joints = q.size();

MatrixXd J(1,joints);
MatrixXd J_orientation = J_ori(q);
MatrixXd J_position = J_pos(q);
MatrixXd T = directkinematics(q);
Matrix3d R = T.block(0,0,3,3);
Vector3d p_ee = T.block(0,3,3,1);
VectorXd axis = R2axis(R);
Vector3d a;

a << axis(1), axis(2), axis(3);

Vector3d ad;

ad = (p - p_ee) / (p - p_ee).norm();


J =  ((ad -a).transpose() / (ad -a).norm()) * (-skew(ad) * skew(p - p_ee) * J_position + skew(a) * J_orientation);

return J; */

	int joints = q.size();

	MatrixXd J(1,joints);
	MatrixXd J_orientation = J_vehicle_ori(q);
	MatrixXd J_position = J_vehicle_pos(q);

	Vector3d rpy;
	rpy << q(3), q(4), q(5);

	Matrix3d R = rpy2rot(rpy);
	Vector3d p_ee;
	p_ee << q(0), q(1), q(2);
	VectorXd axis = R2axis(R);
	Vector3d a;

	//a << axis(1), axis(2), axis(3);
	a << axis(0), axis(1), axis(2);

	Vector3d ad;

	ad = (p - p_ee) / (p - p_ee).norm();

	Matrix3d S(3,3);
	S = skew(p - p_ee).transpose() * (skew(p - p_ee)*skew(p - p_ee).transpose()).inverse();
	//S = skew(p - p_ee);
	double app = (ad-a).norm();
	if (app < pow(10,-4))
		app = pow(10,-4);

	J =  -((ad -a).transpose() / app) * (-skew(ad) * S * J_position + skew(a) * J_orientation);
	//cout<<"check J"<<endl;
	return J;


}


MatrixXd J_vehicle_ori(VectorXd q){
/*
int joints = q.size();

double cf = cos(q(3));
double sf = sin(q(3));

double st = sin(q(4));
double ct = cos(q(4));

double tt = tan(q(4));

MatrixXd J(3,joints);



Matrix3d T;

T << 1, sf*tt, cf*tt,
     0,cf, -sf,
     0, sf/ct, cf/ct;


J.block(0,0,3,3).setZero();

J.block(0,3,3,3)  << 1,0, -st,
      		     0,cf, ct*sf,
     		     0,-sf, ct*cf;
     		     
   	     
J.block(0,3,3,3) =T;



return J; */


int joints = q.size();

	double cf = cos(q(3));
	double sf = sin(q(3));

	double st = sin(q(4));
	double ct = cos(q(4));

	MatrixXd J(3,joints);


	J.block(0,0,3,3).setZero();

	//J.block(0,3,3,3)  << 1,0, -st,
	//     		     0,cf, ct*sf,
	//    		     0,-sf, ct*cf;
	//

	// q(5) = fhi -> roll         q(4) = theta -> pitch

	double tang_t = tan(q(4));
	
	/*
	J.block(0,3,3,3)  << 1, sf*tang_t, cf*tang_t,
					0,    cf    ,   -sf    ,
					0,  sf/ct   ,  cf/ct   ;
	*/

	J.block(0,3,3,3)  << 1, sf*(st/ct), cf*(st/ct),
					0,    cf    ,   -sf    ,
					0,  sf/ct   ,  cf/ct   ;

	/*
	J.block(0,3,3,3)  << 1, 0, 0,
	     				0, cf, 0,
						0, -sf, 1;
	*/

	return J;


}



MatrixXd J_vehicle_pos(VectorXd q){


int joints = q.size();
Vector3d rpy;

rpy(0) = q(3);
rpy(1) = q(4);
rpy(2) = q(5);


Matrix3d R;
R = rpy2rot(rpy);

MatrixXd J(3,joints);

J.block(0,0,3,3)  << R;

J.block(0,3,3,3).setZero();



return J;

}


MatrixXd J_vehicle_oripos(VectorXd q){

int joints = q.size();
MatrixXd J(6,joints);


J.block(0,0,3,6) = J_vehicle_pos(q);

J.block(3,0,3,6) = J_vehicle_ori(q);


return J;


}

MatrixXd J_vehicle_attitude(VectorXd q){

int joints = q.size();

MatrixXd J(2,joints);

J.row(0) = J_vehicle_ori(q).row(0);
J.row(1) = J_vehicle_ori(q).row(1);


return J;

}

MatrixXd J_vehicle_altitude(VectorXd q){


int joints = q.size();

MatrixXd J(1,joints);

J.row(0) = J_vehicle_pos(q).row(2);

return J;

}

MatrixXd J_vehicle_obstacle(VectorXd q, Vector3d p_obst){

int joints = q.size();

MatrixXd J(1, joints);

double dist = sqrt((p_obst - value_vehicle_pos(q)).transpose() * (p_obst - value_vehicle_pos(q)));

    MatrixXd J_p(3,joints);
    J_p = J_vehicle_pos(q);

  
  
   if(dist < pow(10,-3))
        dist = pow(10,-3);

    J = - ((p_obst - value_vehicle_pos(q)).transpose()/dist)*J_p;

return J;

}


MatrixXd inv_J_vehicle_oripos(VectorXd q){

MatrixXd J = J_vehicle_oripos(q);

MatrixXd invJ(6,6);

invJ.block(0,0,3,3) = J.block(0,0,3,3).transpose();
invJ.block(0,3,3,3).setZero();
invJ.block(3,0,3,3).setZero();
invJ.block(3,3,3,3) = J.block(3,3,3,3).inverse();

return invJ;

}


MatrixXd T_uvms(){


MatrixXd T(4,4);

T.block(0,0,3,3).setIdentity();
T.block(0,3,3,1) << 0,0,1;
T.row(3) << 0,0,0,1;

return T;

}

MatrixXd J_uvms_ee_pos(VectorXd q){

int joints = q.size();
int arm_joints = q.size() - 6;

VectorXd q_arm(arm_joints);
VectorXd q_vehicle(6);

q_arm = q.tail(6);
q_vehicle = q.head(6);

Vector3d rpy;

rpy(0) = q(3);
rpy(1) = q(4);
rpy(2) = q(5);


Matrix3d R;
R = rpy2rot(rpy);

MatrixXd T(4,4);

T = T_uvms();
Vector3d rbb0 = T.block(0,3,3,1);


MatrixXd J(3,joints);

J.block(0,0,3,3) = R;
J.block(0,3,3,3) = - ( S(R*rbb0));
/* DA FINIRE */



return J;

}

Matrix3d S(Vector3d x){

Matrix3d out;

out << 0, -x(2), x(1),
       x(2), 0, -x(0),
       -x(1), x(0), 0;

return out;

}

MatrixXd J_uvms_vehicle_pos(VectorXd q){

int joints = q.size();
int arm_joints = q.size() - 6;

MatrixXd J(3,joints);

J.block(0,0,3,arm_joints).setZero();
J.block(0,arm_joints,3,6) << J_vehicle_pos(q.tail(6));



return J;


}

MatrixXd J_FoV(VectorXd q, Vector3d p_obj){



	int j=0;
	Vector3d pos_o;
	pos_o = p_obj;

	Vector3d q_pos;
	q_pos <<  q(0), q(1), q(2);
	/*
	Vector3d ad;

	ad = (p_obj - q_pos)/((p_obj - q_pos).norm());
	Vector3d a;
	*/




	double sigma_=0;
	MatrixXd J_ = MatrixXd::Zero(1,6);
  	MatrixXd J(1,6);

	Vector3d rpy;
	rpy << q(3), q(4), q(5);
	Matrix3d R_c;
	Vector3d r;

	R_c = rpy2rot(rpy);
	r = -R_c.transpose()*(q_pos-pos_o);

	double r_i = r(j);
	double r_norm =r.norm();
	
	/*
	if (r_norm ==0){
			r_norm = 0.0001;
	} 
	*/

	//double sigma_no_abs =PI/2 - std::acos(r_i/r_norm);
	double sigma_no_abs = std::acos(r_i/r_norm);
	sigma_ = std::abs(sigma_no_abs);

	Vector3d e_i= Vector3d::Zero();
	if (j==0) {
			e_i = Vector3d(1,0,0);
	}else if (j==1) {
			e_i = Vector3d(0,1,0);
	}else if (j==2) {
			e_i = Vector3d(0,0,1);
	}

	Matrix3d appo_1 = Matrix3d::Identity() - (r*r.transpose())/(r_norm*r_norm);
	MatrixXd appo_2;
	appo_2.resize(3,6);
	appo_2.block(0,0,3,3) = R_c.transpose();
	appo_2.block(0,3,3,3) = skew(r);



	MatrixXd J_Camera(6, 6);
	J_Camera = J_vehicle_oripos(q);

	//J_ = sigma_no_abs/sigma_*e_i.transpose()*appo_1*appo_2*J_Camera/sqrtl(r_norm*r_norm-(r_i*r_i));
	//J_ = -e_i.transpose()*appo_1*appo_2*J_Camera/sqrtl(r_norm*r_norm-(r_i*r_i));


	//cout<<"r_norm*r_norm-(r_i*r_i): "<<r_norm*r_norm-(r_i*r_i)<<endl;
	appo_2.block(0,0,3,3) = -appo_1*R_c.transpose();
	appo_2.block(0,3,3,3) = skew(r);

  	double appo_3 = r_norm*r_norm-(r_i*r_i);
	if(appo_3 < pow(10,-12))
			appo_3 = pow(10,-12);

	J_ = -e_i.transpose()*appo_2*J_Camera/sqrtl(appo_3);

	//J_(0,3) = 0;



	//*sigma = sigma_;
	J = J_;

  	return J;

}

MatrixXd J_rollFixed(VectorXd q){

	int joints = q.size();

	MatrixXd J(1,joints);

	J.row(0) = J_vehicle_ori(q).row(0);

	return J;
}

MatrixXd J_vehicle_heading(VectorXd q){

	int joints = q.size();

	MatrixXd J(2,joints);

	J.row(0) = J_vehicle_ori(q).row(1);
	J.row(1) = J_vehicle_ori(q).row(2);


	return J;

}

MatrixXd J_vehicle_depth(VectorXd q){


	int joints = q.size();

	MatrixXd J(1,joints);

	J.row(0) = J_vehicle_pos(q).row(2);

	return J;

}

MatrixXd J_vehicle_altitude_setBased(VectorXd q){


	int joints = q.size();

	MatrixXd J(1,joints);

	J.row(0) = J_vehicle_pos(q).row(2);

	return J;

}



