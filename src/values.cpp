#include "LLkinematics.h"

VectorXd value_oripos(VectorXd q){

/* Value of the end-effector configuration task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	oripos		dim: 7			vector stacking [position, quaternion]
	

*/



MatrixXd T(4,4);

T = directkinematics(q);

Vector3d pos;
VectorXd ori(4);
VectorXd oripos(7);
Matrix3d R;

pos = T.block(0,3,3,1);

R = T.block(0,0,3,3);
ori = rot2quat(R);

oripos.head(3) = pos;
oripos.tail(4) = ori;

return oripos;

}

VectorXd value_pos(VectorXd q){

/* Value of the end-effector position task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	pos		dim: 3			e-e position
	

*/


MatrixXd T(4,4);

T = directkinematics(q);


Vector3d pos;

pos = T.block(0,3,3,1);

return pos;

}

VectorXd value_ori(VectorXd q){

/* Value of the end-effector orientation task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	ori		dim: 4			e-e quaternion
	

*/


MatrixXd T(4,4);
Matrix3d R;
VectorXd ori(4);

T = directkinematics(q);

R = T.block(0,0,3,3);

ori = rot2quat(R);

return ori;


}

VectorXd value_obst_wrist(Vector3d p_obst, VectorXd q){

/* Value of the end-effector obstacle avoidance at wrist task.

 input: 
 	
 	Vector3d p_obst			dim: 3x1		obstacle position
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			distance between the obstacle and the wrist
	

*/


MatrixXd T(4,4);
T = directkinematics(q);
Vector3d pos;

pos = T.block(0,3,3,1);
VectorXd value(1);

value(0) = sqrt((p_obst - pos).transpose() * (p_obst - pos));

return value;
}

VectorXd value_obst_elbow(Vector3d p_obst, VectorXd q, int id){

/* Value of the end-effector obstacle avoidance at elbow task.

 input: 
 	
 	Vector3d p_obst			dim: 3x1		obstacle position
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim: 1			index of the elbow joint
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			distance between the obstacle and the elbow
	

*/

//cout<<"q-function-value: "<<q<<endl;
MatrixXd T(4,4);
T = directkinematics(q, id);
Vector3d pos;

//cout<<"T: "<<T<<endl;

pos = T.block(0,3,3,1);
VectorXd value(1);
/*
	cout<<"p_obst(0): "<<p_obst(0)<<endl;
	cout<<"p_obst(1): "<<p_obst(1)<<endl;
	cout<<"p_obst(2): "<<p_obst(2)<<endl;

	cout<<"pos(0): "<<pos(0)<<endl;
	cout<<"pos(1): "<<pos(1)<<endl;
	cout<<"pos(2): "<<pos(2)<<endl;
*/
value(0) = sqrt((p_obst - pos).transpose() * (p_obst - pos));
//cout<<"value function: "<<value<<endl;
return value;

}

VectorXd value_jointlimit(VectorXd q, int id){

/* Value of the joint limit task.

 input: 
 	
	Eigen::VectorXd q		dim: nx1		joint positions
	int id				dim: 1			index of the joint
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			joint value
	

*/


VectorXd value(1);

if(id >= q.size()){

	cout << "\n[value_jointlimit function]: the system has " << q.size() << " joints, requested id = " << id+1 << endl; 
	exit(0);

}


value(0) = q(id);

return value;

}

VectorXd value_manipulability(VectorXd q){

/* Task value of the arm manipulability  task.

 input: 
 
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	VectorXd w			dim: 1			manipulability measure
	

*/

        int i=0, j=0, joints = q.size();
        MatrixXd J(6,joints);
        J = J_oripos(q);
        MatrixXd Jac(6,joints);

        

        double det = (J*J.transpose()).determinant();
        VectorXd  w(1);
        w(0)	 = sqrt(det);
        return w;
    }

    
VectorXd value_wall(Vector3d p1, Vector3d p2, Vector3d p3, Vector3d p_d, VectorXd q){

/* Value of the virtual plane task.

 input: 
 
 	Eigen::Vector3d p1		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p2		dim: 3x1		x,y,z coordinates of a point belonging to the plane
 	Eigen::Vector3d p3		dim: 3x1		x,y,z coordinates of a point belonging to the plane
	Eigen::VectorXd q		dim: nx1		joint positions
	
	
 output:
 
 	Eigen::VectorXd	value		dim: 1			distance between the wall and the end-effector	

*/


MatrixXd T(4, 4);
Vector3d pos;
VectorXd value(1);
 
T = directkinematics(q);
pos = T.block(0,3,3,1);

Vector3d plane_normal;
plane_normal << ((p2-p1).cross(p3-p1)) / ((p2-p1).cross(p3-p1)).norm();

value(0) = abs(plane_normal.transpose() *(pos - p2));



return value;

}



VectorXd value_fov(Vector3d p, VectorXd q){

	//cout<<"p in value: "<<p<<endl;

	VectorXd value(1);

	Vector3d rpy;
	rpy << q(3), q(4), q(5);
	Matrix3d R = rpy2rot(rpy);
	Vector3d p_ee;
	p_ee << q(0), q(1), q(2);
	VectorXd axis = R2axis(R);
	Vector3d a;

	//a << axis(1), axis(2), axis(3);
	a << axis(0), axis(1), axis(2);

	Vector3d ad;

	ad = (p - p_ee) / (p - p_ee).norm();
    cout<<"ad: "<<ad<<endl;
	cout<<"a: "<<a<<endl;

	value(0) = sqrt( (ad - a).transpose() * (ad - a));
    //cout<<"check value"<<endl;
	return value;

	/*
	VectorXd value(1);

	MatrixXd T = directkinematics(q);
	Matrix3d R = T.block(0,0,3,3);
	Vector3d p_ee = T.block(0,3,3,1);
	VectorXd axis = R2axis(R);
	Vector3d a;

	a << axis(1), axis(2), axis(3);

	Vector3d ad;

	ad = (p - p_ee) / (p - p_ee).norm();


	value(0) = sqrt( (ad - a).transpose() * (ad - a));

	return value;
	*/

}


VectorXd value_vehicle_pos(VectorXd q){

	Vector3d value, pos;
	/*
	Vector3d rpy, pos;
	rpy << q(5), q(4), q(3);
	rpy << q(3), q(4), q(5);

	Matrix3d R = rpy2rot(rpy);
	*/
	pos << q(0), q(1), q(2);


	value = pos;


	return value;

}


VectorXd value_vehicle_ori(VectorXd q){

Vector3d value;

value << q(3), q(4), q(5);

return value;


}


VectorXd value_vehicle_oripos(VectorXd q){

	VectorXd value(6);

	Vector3d rpy, pos;
	rpy << q(5), q(4), q(3);
	rpy << q(3), q(4), q(5);

	Matrix3d R = rpy2rot(rpy);
	pos << q(0), q(1), q(2);


	value.head(3) = value_vehicle_pos(q);

	value.tail(3) = value_vehicle_ori(q);

	return value;


}

VectorXd value_vehicle_attitude(VectorXd q){

VectorXd value(2);

//value << q(5), q(4);
value << q(3), q(4);
return value;

}

VectorXd value_vehicle_altitude(VectorXd q){

Vector3d value;

value << q(2);

return value;

}


VectorXd value_vehicle_obstacle(VectorXd q,Vector3d  p_obst){

VectorXd value(1);

value(0) = sqrt((p_obst - value_vehicle_pos(q)).transpose() * (p_obst - value_vehicle_pos(q)));

return value;

}

VectorXd value_uvms_ee_pos(VectorXd q){

int arm_joints = q.size() - 6;
int joints = q.size();

VectorXd value(3);

MatrixXd T_arm(4,4), T_out(4,4), T_vehicle(4,4), T_tran(4,4);
Matrix3d R_vehicle;
Vector3d rpy;

T_arm = directkinematics(q.head(arm_joints));

T_tran = T_uvms();

rpy << q( q.size() - 1), q( q.size() - 2), q( q.size() - 3);
R_vehicle = rpy2rot(rpy);
T_vehicle.block(0,0,3,3) = R_vehicle;
T_vehicle.block(0,3,3,1) << q(q.size() - 6), q(q.size() - 5), q(q.size() - 4);
T_vehicle.row(3) << 0,0,0,1; 

T_out = T_vehicle * T_tran * T_arm;

value = T_out.block(0,3,3,1);


return value;

}


VectorXd value_uvms_vehicle_pos(VectorXd q){


int arm_joints = q.size() - 6;
int joints = q.size();


Vector3d value;

Vector3d rpy, pos;
rpy << q( q.size() - 1), q( q.size() - 2), q( q.size() - 3);

Matrix3d R = rpy2rot(rpy);
pos << q(q.size() - 6), q(q.size() - 5), q(q.size() - 4);


value = R*pos;



return value;

}

VectorXd value_FoV(VectorXd q, Vector3d  p_obj){

cout<<"check FoV error1"<<endl;
	int j=0;
 	Vector3d pos_o;
  	pos_o = p_obj;

	Vector3d q_pos;
	q_pos <<  q(0), q(1), q(2);

 	double sigma_=0;
 	MatrixXd J_ = MatrixXd::Zero(1,6);
	MatrixXd J(1,6);

 	Vector3d rpy;
 	rpy << q(3), q(4), q(5);
 	Matrix3d R_c;
 	Vector3d r;
cout<<"check FoV error2"<<endl;
 	R_c = rpy2rot(rpy);
	cout<<q_pos-pos_o<<endl;
	cout<<-R_c.transpose()<<endl;
cout<<-R_c.transpose()*(q_pos-pos_o)<<endl;
	  cout<<"check FoV error9"<<endl;
	
 	r = -R_c.transpose()*(q_pos-pos_o);
 cout<<"check FoV error10"<<endl;
 	double r_i = r(j);
	 cout<<"check FoV error11"<<endl;
	 
 	double r_norm =r.norm();
  cout<<"check FoV error3"<<endl;
  	/*
	if (r_norm ==0){
 			r_norm = 0.0001;
 	}
  	*/

	// double sigma_no_abs =PI/2 - std::acos(r_i/r_norm);
	//cout<<"r_i/r_norm: " <<r_i/r_norm<<endl;
	double sigma_no_abs = std::acos(r_i/r_norm);

	//cout<<"std::acos(r_i/r_norm): "<<std::acos(r_i/r_norm)*180/PI<<endl;
	//cout<<"sigma_no_abs: "<<sigma_no_abs*180/PI<<endl;
 	sigma_ = std::abs(sigma_no_abs);
	sigma_ = (sigma_no_abs);
cout<<"check FoV error4"<<endl;
 	Vector3d e_i= Vector3d::Zero();
 	if (j==0) {
 			e_i = Vector3d(1,0,0);
 	}else if (j==1) {
 			e_i = Vector3d(0,1,0);
 	}else if (j==2) {
 			e_i = Vector3d(0,0,1);
 	}
cout<<"check FoV error4"<<endl;
 	VectorXd value(1);
 	value(0) = sigma_;
	cout<<"check FoV error"<<endl;
	return value;

}

VectorXd value_rollFixed(VectorXd q){

	VectorXd value(1);

	//value << q(5), q(4);
	value << q(3);

	return value;

}

VectorXd value_vehicle_heading(VectorXd q){

	VectorXd value(2);

	value << q(4), q(5);

	return value;

}

VectorXd value_vehicle_depth(VectorXd q){

	VectorXd value(1);

	value << q(2);

	return value;

}

VectorXd value_vehicle_altitude_setBased(VectorXd q, double dist){

	VectorXd value(1);

	value << dist;

	return value;

}



