#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include "LLkinematics.h"
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Dense>

using namespace Eigen;
using namespace std;

double ***jaco_directkinematics(VectorXd q){

/* 
	Direct kinematics for the Jaco2 (7 DoF) arm
	
	input:
		Eigen::VectorXd q	dim: nx1	joint positions
		
	output:
	
		double ***T0		dim: 4x4xn	homogeneous transform matrix

*/

    int i=0, z=0, j=0;
    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T, ***T0;
    double **DH;
    
    if(joints == 6)
    
    	DH = readDH1(joints);
    	
    else if(joints == 7)
    
    	DH = readDH2(joints);
    	
    DH = elaborateDH(joints, q1, DH);
    T = elaborateT(DH, joints);
    T0 = elaborateT0(T, joints);


    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray2d(joints,DH);
    
    
    return T0;
}
