//#include <Task.h>
//#include <TaskHierarchy.h>
#include <ExtendedTaskHierarchy.h>


ExtendedTaskHierarchy::ExtendedTaskHierarchy(){}

ExtendedTaskHierarchy::~ExtendedTaskHierarchy(){

deleteTree(tree);
delete tree;

}

TaskHierarchy ExtendedTaskHierarchy::getTaskHierarchy(){

return taskHierarchy;

}

TaskHierarchy ExtendedTaskHierarchy::getActiveTasks(){

return activeTasks;

}

void ExtendedTaskHierarchy::setTaskHierarchy(TaskHierarchy h){


taskHierarchy = h;
}

void ExtendedTaskHierarchy::printTaskHierarchy(){


taskHierarchy.print();
}

void ExtendedTaskHierarchy::printActiveTasks(){

activeTasks.print();
}


int ExtendedTaskHierarchy::treeNodes(node *curr){

int count=0;

if(curr==NULL)
	return 0;


else 

	return(1+treeNodes(curr->left)+treeNodes(curr->right));

}

int ExtendedTaskHierarchy::treeLeaves(node *cur){

if (cur == NULL)
	return 0;

else{


	if(cur->left==NULL && cur->right==NULL)
		return(1);
	else return(treeLeaves(cur->left)+treeLeaves(cur->right));

}

}

VectorXd ExtendedTaskHierarchy::getTaskError(int id, VectorXd q){

return taskHierarchy.getTaskError(id,q);


}

VectorXd ExtendedTaskHierarchy::getTaskValue(int id, VectorXd q){

return taskHierarchy.getTaskValue(id, q);

}


void ExtendedTaskHierarchy::setTaskParameters(int i, vector<double> p){


taskHierarchy.setTaskParameters(i,p);
}

void ExtendedTaskHierarchy::setActiveTasks(VectorXd q){

vector<double> params(2);
Task dummy;

for(int i=0; i<taskHierarchy.size();i++){

dummy = taskHierarchy[i];

	if(dummy.isSetBased() == 0 )

		activeTasks.insert(dummy);

	if (dummy.isSetBased() == 1 ){

		params = dummy.getParams();

		if(dummy.taskValue(q)(0) < params[0]){
			
			dummy.setThresholdViolated(0);
			activeTasks.insert(dummy);
		
		
			
		}

		if(dummy.taskValue(q)(0)> params[1]){
			
			
			dummy.setThresholdViolated(1);
			activeTasks.insert(dummy);
		
		
		}
		
	}

}


}



int ExtendedTaskHierarchy::numberofSetBased(){

int count=0;

for(int i=0;i<activeTasks.size();i++)

	if(activeTasks[i].isSetBased() == 1)

			count++;


return count;

}

vector<VectorXd> ExtendedTaskHierarchy::computeSolutions(TaskHierarchy h){


int n = numberofSetBased();

vector<VectorXd> solutions(pow(2,n));

vector<TaskHierarchy> dummy(pow(2,n));

vector<double> par;
MatrixXd K;


int count=0;

for(int i=0;i<activeTasks.size();i++){


if (activeTasks[i].isSetBased() == 0){

for(count=0;count<solutions.size();count++)
	
	dummy[count].insert(activeTasks[i]);

}





}




return solutions;

}

void ExtendedTaskHierarchy::buildTree(TaskHierarchy hierarchy, int i, node *cur){


if (cur == NULL){
	
	return;

}
	
if(i == hierarchy.size()){

	cur->left=NULL;
	cur->right=NULL;


	return;

}


if(hierarchy[i].isSetBased() == 0){

	cur->left = new node;
	cur->right = new node;

	cur->left->h=cur->h;
	cur->left->h.insert(hierarchy[i]);
	cur->right=NULL;

	
}

if(hierarchy[i].isSetBased() == 1){

	cur->left = new node;
	cur->right = new node;

	cur->left->h = cur->h;
	cur->right->h = cur->h;
	cur->left->h.insert(hierarchy[i]);

}
	

buildTree(hierarchy, i+1, cur->left);
buildTree(hierarchy, i+1, cur->right);


}


void ExtendedTaskHierarchy::printSolutions(node *t){

if (t==NULL)
	return;

if(t->left==NULL && t->right==NULL){

	cout << "\n\nSOLUZIONE\n\n";
	for(int i=0;i<t->h.size();i++)
		cout  <<  t->h[i].getType() << "\t";
	cout << "\n\n";
}

else{

	printSolutions(t->left);
	printSolutions(t->right);

}

}




void ExtendedTaskHierarchy::printTree(node *cur){


	if (cur==NULL)
		return

cur->h.print();



}


void ExtendedTaskHierarchy::fillSolutionsVector(node *t, vector<TaskHierarchy> &v, vector<VectorXd> &solutions, VectorXd q){



if (t==NULL)
	return;

if(t->left==NULL && t->right==NULL){

	v.push_back(t->h);
	
	solutions.push_back(t->h.computeNSBsolution(q));


}

else{

	fillSolutionsVector(t->left, v, solutions,q);
	fillSolutionsVector(t->right, v, solutions,q);

}


}


vector<VectorXd> ExtendedTaskHierarchy::evaluateProjections(TaskHierarchy h, vector<VectorXd> &solutions, vector<TaskHierarchy> v, VectorXd q){


int N_sol = solutions.size();
int N_task = h.size();


vector<VectorXd> possible_sol; 




if(N_sol == 1)

	possible_sol.push_back(solutions[0]);




for(int i=0;i<N_sol;i++){

int count=0;
	for(int j=0;j<N_task;j++){

if(h[j].isSetBased() == 0){
		

	count++;
	if(count == N_task-1){

		possible_sol.push_back(solutions[i]);
	
		
	}

	else
		continue;
}
		
 else{

		if(v[i].findTask(h[j]) == true){


			count++;
			if(count == N_task-1){

				possible_sol.push_back(solutions[i]);
			
				
			}

			else
				continue;

		}


		if(h[j].getThresholdViolated() == 0){
			


			if((h[j].Jacobian(q) * solutions[i])(0) > 0){
			
			count++;
				if(count == N_task-1){

					possible_sol.push_back(solutions[i]);
				
					
				}
	
				else
					continue;
			}
	
			else{
	
				break;
				
			}

		}

		if(h[j].getThresholdViolated() == 1){




			if((h[j].Jacobian(q) * solutions[i])(0) < 0){
			
			count++;

				if(count == N_task-1){

					possible_sol.push_back(solutions[i]);
					
				}
	
				else
					continue;
					
			
			}
	
			else{
				
	
				break;
				
			}

		}

}
	


			
	}


}




if(possible_sol.size()==0){
	possible_sol.push_back(solutions[0]);

	cout << "check\n";
}



return possible_sol;



}


VectorXd ExtendedTaskHierarchy::chooseSolution(vector<VectorXd> sol){


VectorXd dq;

//cout << "\nPossibili sol: " << sol.size();

/*
double max=-1;
int id=-1;

for(int i=0; i<sol.size();i++){



if(sol[i].norm() > max){
		
	max=sol[i].norm();
	id=i;
}


}
*/



dq = sol[sol.size()-1];
/*
for(int j =0;j<7;j++){

if(isnan(dq(j)) == true)
	dq << 0,0,0,0,0,0,0;
}

if(dq.norm() > 100)
	dq << 0,0,0,0,0,0,0;
*/
/*
cout << "\n\n<<<<<<<<<<<<<<<\n\n";
		cout <<dq << "\n\n";
		sleep(2);
*/
return dq;

}

VectorXd ExtendedTaskHierarchy::computeDq(VectorXd q){

int joints = q.size();
VectorXd dq(joints);

vector<VectorXd> tree_solutions, possible_solutions;
vector<TaskHierarchy> v, out;
TaskHierarchy dummy;

tree=new node;



setActiveTasks(q);

for(int i = 0; i<activeTasks.size();i++){

	if(activeTasks[i].getType() == 12)
cout << "\n\n\nSettati task attivi " << activeTasks.size() << "\n";
}

buildTree(activeTasks,0,tree);
//cout << "\nAlbero costruito";

fillSolutionsVector(tree, v, tree_solutions, q);
//cout << "\nSoluzioni calcolate";

possible_solutions=evaluateProjections(activeTasks, tree_solutions, v, q);
//cout << "\nSoluzioni proiettate";
dq=chooseSolution(possible_solutions);


deleteTree(tree);


clearActiveTasks();

return dq;
}


void ExtendedTaskHierarchy::deleteTree(node *n){



if ( n != NULL ) {
    deleteTree ( n->left );
    deleteTree ( n->right );
    delete n;
  }
  
  
}







void ExtendedTaskHierarchy::clearActiveTasks(){


activeTasks.clear();

}

Task ExtendedTaskHierarchy::getTask(int i){

	return taskHierarchy[i];

}

int ExtendedTaskHierarchy::size(){


return taskHierarchy.size();

}



VectorXd ExtendedTaskHierarchy::optimize(TaskHierarchy in, VectorXd q){

VectorXd dq(7);
int no_of_tasks = in.size();

if(no_of_tasks == q.size()){

	dq = in.computeNSBsolution(q);

	
}

return dq;


}
