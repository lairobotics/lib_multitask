#include "LLkinematics.h"


using namespace Eigen;
using namespace std;

MatrixXd directkinematics(VectorXd q)
{

    /* 
        Direct kinematics for the Jaco2 (7 DoF) arm
        
        input:
            Eigen::VectorXd q	dim: nx1	joint positions
            
        output:
        
            Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the end-effector to the base

    */

    int i=0, z=0, j=0;
    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T, ***T0;
    
    double **DH;
    
    if(joints == 6)
    
    	DH = readDH1(joints);
    	
    else if(joints == 7)
    
    	DH = readDH2(joints);
    	
    DH = elaborateDH(joints, q1, DH);
    T = elaborateT(DH, joints);
    T0 = elaborateT0(T, joints);

    MatrixXd T_out(4,4);
    
    for(i=0;i<4;i++)
    	for(j=0;j<4;j++)
    	
    		T_out(i,j) = T0[i][j][joints-1];
    		
    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray3d(4,4,T0);
    deallocateArray2d(joints,DH);
    
    
    return T_out;
}

/*
MatrixXd mydirectkinematics_wrist(VectorXd q)
{

    /* 
        Direct kinematics for the Jaco2 (7 DoF) arm
        
        input:
            Eigen::VectorXd q	dim: nx1	joint positions
            
        output:
        
            Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the end-effector to the base

    */
    /*
    int i=0, z=0, j=0;
    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T, ***T0;
    
    double **DH;
    
    if(joints == 6)
    
    	DH = readDH1(joints);
    	
    else if(joints == 7)
    
    	DH = readDH2(joints);
    	
    DH = elaborateDH(joints, q1, DH);
    T = elaborateT(DH, joints);
    T0 = elaborateT0(T, joints);

    MatrixXd T_out(4,4);
    
    for(i=0;i<4;i++)
    	for(j=0;j<4;j++)
    	
    		T_out(i,j) = T0[i][j][joints-2];
    		
    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray3d(4,4,T0);
    deallocateArray2d(joints,DH);
    
    
    return T_out;
}
*/



MatrixXd directkinematics(VectorXd q, int id)
{
    //cout<<"q: "<<q<<endl;
    /* 
        Direct kinematics for the Jaco2 (7 DoF) arm
        
        input:
            Eigen::VectorXd q	dim: nx1	joint positions
            int id			dim: 1		joint id
            
        output:
        
            Eigen::MatrixXd T_out	dim: 4x4	homogeneous transform matrix from the link "id" to the base

    */

    if(id >= q.size()){

        cout << "\n[directkinematics function]: the system has " << q.size() << " joints, requested id = " << id+1 << endl; 
        exit(0);

    }


    int i=0, z=0, j=0;
    int joints = q.size();

    double *q1 = VectorXdtoarray1d(q);

    double ***T, ***T0;
    double **DH;

    if(joints == 6)
    
    	DH = readDH1(joints);
    	
    else if(joints == 7)
    {    
    	DH = readDH2(joints);
       // cout<<"directkinematics CHECK"<<endl;
    }

    DH = elaborateDH(joints, q1, DH);
    T = elaborateT(DH, joints);
    T0 = elaborateT0(T, joints);

    MatrixXd T_out(4,4);
    
    for(i=0;i<4;i++)
    {
    	for(j=0;j<4;j++)
    	{
    		T_out(i,j) = T0[i][j][id];
          //  cout<<"T_out: "<<T_out(i,j)<<endl;
        }
    }		
    delete[] q1;
    deallocateArray3d(4,4,T);
    deallocateArray3d(4,4,T0);
    deallocateArray2d(joints,DH);
    

    return T_out;
}

