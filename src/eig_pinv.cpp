#include <iostream>
#include <stdlib.h>
#include "LLkinematics.h"
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <math.h>

using namespace std;
using namespace Eigen;


MatrixXd eig_pinv(MatrixXd J, double threshold, double lambda){

	int rowJ = J.rows();
	int colJ = J.cols();
	MatrixXd pinvJ(colJ, rowJ);

	JacobiSVD<MatrixXd> svd(J, ComputeFullU | ComputeFullV);

	VectorXd eigValues(rowJ);
	complex<double> app;
	for(int i=0; i<rowJ; i++){
		app = svd.singularValues()(i);
		eigValues(i) = app.real();
	}
	
	
	VectorXd p(rowJ);
	MatrixXd Sinv(colJ, rowJ);
	Sinv.setZero();
	for(int i=0; i<rowJ; i++){
	
		p(i) = cosRialzato(eigValues(i), lambda, threshold);
	
		Sinv(i,i) = eigValues(i)/(pow(eigValues(i),2) + p(i));
	
	}

if(J.rows() == 1){

//cout << "J: \n" << J << "\n\n";
//cout << "V: \n" << svd.matrixV() << "\n\nSinv\n" << Sinv << "\n\nU trasposto: \n" << svd.matrixU().transpose() << "\n\n\n\n\n";

}

	pinvJ = svd.matrixV() * Sinv * svd.matrixU().transpose();			
	
	return pinvJ;

}

double cosRialzato(double sigma, double lambda, double threshold){


	double tmp, reg;
	
	sigma = abs(sigma);
	if(sigma < threshold){
	
		tmp = (sigma/threshold) * PI;
		reg = lambda * (0.5 * cos(tmp)+ 0.5);
//		cout << "Damping! " << rand();
		
	}
	
	else{
	//	cout << "No Damping! " << rand();
		reg = 0.0;
	}
//	cout << reg << "\t" << sigma << "\n";
	
	return reg;

}

